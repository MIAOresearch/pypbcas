from pbcas.ast import stats
from pbcas.normalized import Variable, Integer, normalize
import sys
# from pbcas import Variable, Integer
# from pbcas import normalize_by_reduce as normalize


from pbcas.prooflogging import Proof
from pbcas.opb_formula import OPBFormula
from pbcas.cnf_formula import CNFFormula
from pbcas.timed_function import TimedFunction

def rearange(array):
    if len(array) <= 2:
        return array
    else:
        return array[::2] + rearange(array[1::2])

class App:
    def __init__(self, proof):
        self.usedVariableNames = set()
        self.proof = proof
        self.cnf = CNFFormula()
        self.baseLevel = 0

        self.assumeEqs = False
        self.splitRup = True

    def freshVar(self, basename):
        name = basename
        i = 0
        while name in self.usedVariableNames:
            i += 1
            name = "%s.%i"%(basename, i)
        self.usedVariableNames.add(name)
        return Variable(name)

    def reify(self, C, zj):
        geq = zj >> C
        self.proof.reification(geq)
        leq = zj << C
        self.proof.reification(leq)
        return (zj, geq, leq)


    def derive_bound(self, bounds):
        it = iter(bounds)
        C_low = next(it)
        for j,bound in enumerate(it, start = 2):
            C_low = (((j - 1) * C_low) + bound) / j
            ## Optional: produce intermediate steps
            # self.proof.derivation( C_low )
        return C_low

    def ordering(self, reification_larger, reification_smaller, M):
        right_impl  = reification_larger[1]
        var_larger  = reification_larger[0]
        left_impl   = reification_smaller[2]
        var_smaller = reification_smaller[0]

        ordering = (left_impl + right_impl) / M
        self.proof.derivation(ordering)

        self.proof.equals(-1, var_smaller + ~var_larger >= 1)

        return ordering

    def get_variables(self, reifications):
        return list(zip(*reifications))[0]

    def get_lower_bounds(self, reifications):
        return list(zip(*reifications))[1]

    def get_upper_bounds(self, reifications):
        return list(zip(*reifications))[2]

    def fresh_unary(self, in_vars, outvars):
        if not self.assumeEqs: # quick hack for testing how long just the telescoping would take
            reifications = []
            for j in range(1, len(in_vars) + 1):
                reifications.append(self.reify(sum(in_vars) >= j, outvars[j - 1]))
                if j > 1:
                    self.ordering(reifications[-1], reifications[-2], M = len(in_vars))

            if len(in_vars) > 1:
                assert(len(reifications) > 1)

            C_low = self.derive_bound(          self.get_lower_bounds(reifications)  )
            C_up  = self.derive_bound(reversed( self.get_upper_bounds(reifications) ))

            if len(reifications) > 1:
                self.proof.derivation( C_low )
                self.proof.derivation( C_up )

                self.proof.delete(self.get_lower_bounds(reifications))
                self.proof.delete(self.get_upper_bounds(reifications))

        else:
            for var1, var2 in zip(outvars, outvars[1:]):
                self.proof.assume(var1 >= var2)

            C_low = self.proof.assume(sum(in_vars) >= sum(outvars))
            C_up  = self.proof.assume(sum(outvars) >= sum(in_vars))

        return (outvars, C_low, C_up)

    def getS(self, i,j):
        if j > i:
            return Integer(0)
        elif j <= 0:
            return Integer(1)
        else:
            return self.svars[i - 1][j - 1]

    def sequential_encoding(self, C):
        if getattr(C, "isGeq", False):
            lowerBound = C
            upperBound = None
        elif getattr(C, "isLeq", False):
            lowerBound = None
            upperBound = C
        elif getattr(C, "isEQ", False):
            lowerBound = C.getGeq()
            upperBound = C.getLeq()
        else:
            raise ValueError("Expected '=', '>=' or '<=' constraint.")

        lhs = C.lhs
        degree = C.rhs

        computeTill = degree
        if upperBound is not None:
            computeTill += 1

        variables = [x for _, x in lhs]
        varIt = iter(enumerate(variables, start = 1))

        varnames = lambda i,j: "s_{%i,%i}"%(i,j)
        self.svars = [[self.freshVar(varnames(i + 1,j + 1)) for j in range(min(i + 1, computeTill + 1))] for i in range(len(variables))]

        if self.proof is not None:
            self.proof.set_level(self.baseLevel + 1)

            self.blocks = []
            block_out = []

            for i,x in varIt:
                ## if we do not want to truncat the computation we could
                ## build the next binary sum over all previous variables
                # block_in = block_out + [x]
                block_in = block_out[:computeTill] + [x]
                fresh_names = lambda j: "s_{%i,%i}"%(i,j)
                self.blocks.append( self.fresh_unary(block_in, self.svars[i - 1]) )
                block_out = list(self.get_variables(self.blocks)[-1])


            C_low = sum(self.get_lower_bounds(self.blocks))
            C_up  = sum(self.get_upper_bounds(self.blocks))

            self.proof.derivation( C_low )
            self.proof.derivation( C_up  )

            if lowerBound is not None:
                bound = C_up + lowerBound
                self.proof.derivation(bound)

            if upperBound is not None:
                bound = C_low + upperBound
                self.proof.derivation(bound)

            block_vars = self.get_variables(self.blocks)

            self.proof.set_level(self.baseLevel)

        s = lambda i,j: self.getS(i,j)

        last = len(variables)
        variables = list(enumerate(variables, start = 1))
        if self.splitRup:
            variables = rearange(variables)

        for i,x in variables:
            for j in range(1, min(i, computeTill) + 1):
                C = ~x + ~s(i-1,j-1) + s(i,j) >= 1
                if self.proof is not None:
                    self.proof.rup(C)
                self.cnf.add(C)

                C = ~s(i-1,j) + s(i,j) >= 1
                if self.proof is not None:
                    self.proof.rup(C)
                self.cnf.add(C)

                C = x + s(i-1,j) + ~s(i,j) >= 1
                if self.proof is not None:
                    self.proof.rup(C)
                self.cnf.add(C)

                C = s(i-1,j-1) + s(i-1,j) + ~s(i,j) >= 1
                if self.proof is not None:
                    self.proof.rup(C)
                self.cnf.add(C)

        if lowerBound is not None:
            C = s(last, degree) >= 1
            if self.proof is not None:
                self.proof.rup(C)
            self.cnf.add(C)

        if upperBound is not None:
            C = ~s(last, degree + 1) >= 1
            if self.proof is not None:
                self.proof.rup(C)
            self.cnf.add(C)

        if self.proof is not None:
            self.proof.wipe_level(self.baseLevel + 1)

def main(numVars = None, assumeEqs=False, splitRup=True, printStats = True):
    # begin options
    if numVars is None:
        numVars = 200
    k = numVars // 2
    #k = 3
    useFancyNames = True
    prooflogging = True
    # end options

    TimedFunction.start_total_timer()

    if not useFancyNames:
        rename = Variable2IntRenaming()
    else:
        rename = lambda x: x

    formula = OPBFormula(rename = rename)
    card = sum([Variable("y%i"%(i)) for i in range(1, numVars + 1)]) >= k
    card = normalize(card)
    formula.add( card )

    with open("sequential_counter.opb", "w") as opb_file:
        formula.write(opb_file)

    with open("sequential_counter.pbp", "w") as proof_file:
        if prooflogging:
            proof = Proof(proof_file, formula.num_inequalities(), rename = rename)
        else:
            proof = None

        app = App(proof)
        app.assumeEqs = assumeEqs
        app.splitRup = splitRup
        app.sequential_encoding(card)

    with open("sequential_counter.cnf", "w") as cnf_file:
        app.cnf.rename = rename
        app.cnf.write(cnf_file)

    global stats
    if printStats:
        print(stats)
        TimedFunction.print_stats()

if __name__ == '__main__':
    main()