from pbcas.ast import *
from itertools import accumulate, product


def incrementToValues(increments):
    return [0] + list(accumulate(increments))

def valuesToIncrement(values):
    values = sorted(values)

    result = []
    last = 0
    for i in values:
        if i != last:
            result.append(i - last)
            last = i

    return result

def mkAddTree(nodes):
    if len(nodes) == 1:
        return nodes[0]
    elif len(nodes) == 2:
        return Add(nodes)
    else:
        return Add([mkAddTree(nodes[:len(nodes)//2]), mkAddTree(nodes[len(nodes)//2:])])

def naryToTreeBinaryAdd(ast, state = RewriteState()):
    if isinstance(ast, Add) and len(ast) > 2:
        state.changed = True
        values = ast.values
        # Optional: sort by coefficient. Assumes that first argument
        # of multiplication is coefficient.
        values.sort(key = lambda x: x[0])
        return mkAddTree(values)
    return ast


class Reducer(FancyReducer):
    def __init__(self, app, sum_to = None):
        self.sum_geq = 0
        self.sum_leq = 0
        self.sum_to = sum_to
        self.app = app

    def mult(self, node, childs_return):
        new_terms = [(node.values[0], node.values[1])]
        return new_terms

    def turncat_at_sum_to(self, terms):
        if self.sum_to is None:
            return terms

        result = list()
        s = 0
        for a,x in terms:
            if s >= self.sum_to:
                break

            s += a
            result.append((a,x))
        return result

    def add(self, node, childs_return):
        left_terms = self.turncat_at_sum_to(next(childs_return))
        right_terms = self.turncat_at_sum_to(next(childs_return))

        geq, leq, new_terms = self.app.handle_block(left_terms, right_terms, lambda i:"c_{%i}"%(i))
        self.sum_geq = self.sum_geq + geq
        self.sum_leq = self.sum_leq + leq
        return new_terms

class TotalizerEncoder:
    def __init__(self, context):
        self.context = context

    def freshVar(self, basename):
        name = basename
        i = 0
        while name in self.usedVariableNames:
            i += 1
            name = "%s.%i"%(basename, i)
        self.usedVariableNames.add(name)
        return Variable(name)

    def reify(self, C, name):
        zj = self.context.freshVar(name)
        geq = zj >> C
        self.context.proof.reification(geq)
        leq = zj << C
        self.context.proof.reification(leq)
        return (zj, geq, leq)

    def ordering(self, reification_larger, reification_smaller, M):
        right_impl  = reification_larger[1]
        var_larger  = reification_larger[0]
        left_impl   = reification_smaller[2]
        var_smaller = reification_smaller[0]

        ordering = (left_impl + right_impl) / M
        self.context.proof.derivation(ordering)

        self.context.proof.equals(-1, var_smaller + ~var_larger >= 1)

        return ordering

    def get_variables(self, reifications):
        return list(zip(*reifications))[0]

    def case_split(self, z, left_terms, right_terms):
        outer_sum = 0
        for i in range(len(left_terms) + 1):
            inner_sum = 0
            for j in range(len(right_terms) + 1):
                clause = z

                if i < len(left_terms):
                    clause = clause + left_terms[i][1]

                if j < len(right_terms):
                    clause = clause + right_terms[j][1]

                if i > 0:
                    clause = clause + ~left_terms[i - 1][1]

                if j > 0:
                    clause = clause + ~right_terms[j - 1][1]

                inner_sum = inner_sum + self.context.proof.rup(clause >= 1)

            inner_sum = Saturate(inner_sum)
            self.context.proof.derivation(inner_sum)

            outer_sum = outer_sum + inner_sum

        result = Saturate(outer_sum)
        self.context.proof.derivation(result)
        return result


    def fresh_eq(self, left_terms, right_terms, out_names):
        """
        left_terms and right_terms are sorted increments
        """

        A = incrementToValues(t[0] for t in left_terms)
        B = incrementToValues(t[0] for t in right_terms)

        C = set( ( a+b for a,b in product(A,B) ) )
        C = list(sorted(C))

        lhs = sum(a*x for a,x in left_terms) + sum(b*x for b,x in right_terms)
        M = sum(t[0] for t in left_terms) + sum(t[0] for t in right_terms)

        reifications = []
        for c in C:
            if c != 0:
                reifications.append(self.reify(lhs >= c, out_names(c)))
                if len(reifications) > 1:
                    self.ordering(reifications[-1], reifications[-2], M)

        new_terms = list(zip(valuesToIncrement(C), self.get_variables(reifications)))
        rhs = sum(a*x for a,x in new_terms)

        z_geq,geq,_ = self.reify(lhs >= rhs, "z_geq")
        z_leq,leq,_ = self.reify(lhs <= rhs, "z_leq")
        z_eq,_,_ = self.reify(z_geq + z_leq >= 2, "z_eq")

        self.case_split(z_eq, left_terms, right_terms)

        z_geq_true = z_geq >= 1
        self.context.proof.rup(z_geq_true)
        geq = geq + M * z_geq_true

        z_leq_true = z_leq >= 1
        self.context.proof.rup(z_leq_true)
        leq = leq + M * z_leq_true

        self.context.proof.derivation(geq)
        self.context.proof.derivation(leq)

        return geq, leq, new_terms

    def add_clauses(self, left_terms, right_terms, new_terms):
        A = incrementToValues(t[0] for t in left_terms)
        B = incrementToValues(t[0] for t in right_terms)
        C = incrementToValues(t[0] for t in new_terms)

        target = dict()
        successor = dict()
        for i in range(0,len(C)):
            if i > 0:
                target[C[i]] = new_terms[i - 1][1]
            if i < len(C) - 1:
                successor[C[i]] = new_terms[i][1]

        for i,j in product(range(len(A)), range(len(B))):
            # for a \in A, b \in B, c = a + b \in C
            # C >= A + B
            # if A>=a and B>=b then C>=a+b
            if i + j > 0:
                C = target[A[i] + B[j]]
                if i != 0:
                    C = C + ~left_terms[i - 1][1]
                if j != 0:
                    C = C + ~right_terms[j - 1][1]
                C = C >= 1
                self.context.proof.rup(C)
                self.context.cnf.add(C)

            # C <= A + B
            # if A<=a and B<=b then C<=a+b
            # A<=a is same as not A>=succ(a)

            if i < len(A) - 1 or j < len(B) - 1:
                self.context.proof.comment("i:", i, "j:", j)
                C = ~successor[ A[i] + B[j] ]
                if i < len(A) - 1:
                    C = C + left_terms[i][1]
                if j < len(B) - 1:
                    C = C + right_terms[j][1]
                C = C >= 1
                self.context.proof.rup(C)
                self.context.cnf.add(C)




    def handle_block(self, left_terms, right_terms, out_names):
        geq, leq, new_terms = self.fresh_eq(left_terms, right_terms, out_names)
        self.context.proof.comment("block specification:", "left in:", left_terms, "right in:", right_terms, "result:", new_terms)

        self.add_clauses(left_terms, right_terms, new_terms)

        return geq, leq, new_terms

    def sum_only(self, lhs, reducer = None):
        if reducer is None:
            reducer = Reducer(self)

        assert(len(lhs) > 1)
        lhs = naryToTreeBinaryAdd(lhs)
        new_terms = reduce(lhs, reducer)

        self.context.proof.derivation(reducer.sum_geq)
        self.context.proof.derivation(reducer.sum_leq)

        return new_terms

    def encode(self, constraint):
        assert(isinstance(constraint, Geq))

        normalized = normalize_by_reduce(constraint)
        lhs = normalized.lhs

        reducer = Reducer(self, normalized.rhs + 1)
        new_terms = self.sum_only(lhs, reducer)

        bound = constraint + reducer.sum_leq
        self.context.proof.derivation(bound)

        s = 0
        for a,x in new_terms:
            s += a
            if s >= normalized.rhs:
                C = x >= 1
                self.context.proof.rup(C)
                self.context.cnf.add(C)
                break


def main():
    from pbcas.prooflogging import Proof
    from encoder_context import EncoderContext
    from pbcas.opb_formula import OPBFormula
    from pbcas.cnf_formula import CNFFormula

    # begin options
    nVars = 20
    coeffs = nVars * [1]
    k = nVars // 2
    useFancyNames = True
    # end options

    constraint = sum([a * Variable("x%i"%i) for i,a in enumerate(coeffs, start = 1)]) >= k

    if not useFancyNames:
        rename = Variable2IntRenaming()
    else:
        rename = lambda x: x

    formula = OPBFormula(rename = rename)
    formula.add(constraint)

    # card = sum([a * Variable("x%i"%i) for a,i in enumerate(coeffs)]) >= k
    # formula.add( card )

    with open("sequential_counter.opb", "w") as opb_file:
        formula.write(opb_file)

    with open("sequential_counter.pbp", "w") as proof_file:
        proof = Proof(proof_file, formula.num_inequalities(), rename = rename)
        context = EncoderContext(proof)

        encoder = TotalizerEncoder(context)
        encoder.encode(constraint)

    with open("sequential_counter.cnf", "w") as cnf_file:
        context.cnf.rename = rename
        context.cnf.write(cnf_file)

if __name__ == '__main__':
    main()