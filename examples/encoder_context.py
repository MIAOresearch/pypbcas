from pbcas.ast import *
from itertools import accumulate, product

from pbcas.prooflogging import Proof
from pbcas.opb_formula import OPBFormula
from pbcas.cnf_formula import CNFFormula

class EncoderContext:
    def __init__(self, proof, out_cnf = CNFFormula()):
        self.usedVariableNames = set()
        self.proof = proof
        self.cnf = out_cnf
        self.baseLevel = 0

    def freshVar(self, basename):
        name = basename
        i = 0
        while name in self.usedVariableNames:
            i += 1
            name = "%s.%i"%(basename, i)
        self.usedVariableNames.add(name)
        return Variable(name)