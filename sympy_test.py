from sympy import Symbol


def main():
    x = Symbol("x")
    y = Symbol("y")
    a = Symbol("a")

    print(3 * a * x + 2 * y + x >= 1)

if __name__ == '__main__':
    main()