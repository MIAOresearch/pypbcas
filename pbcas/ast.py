from copy import deepcopy
from pbcas.timed_function import TimedFunction


class Infix:
    # see https://code.activestate.com/recipes/384122/
    def __init__(self, function):
        self.function = function
    def __ror__(self, other):
        return Infix(lambda x, self=self, other=other: self.function(other, x))
    def __or__(self, other):
        return self.function(other)
    def __call__(self, value1, value2):
        return self.function(value1, value2)

def travers(elem, f):
    if f(elem) and isinstance(elem, AST):
        first = True
        for val in elem:
            if not first:
                getattr(f, "infix", lambda: None)()
            first = False
            travers(val, f)
    getattr(f, "postfix", lambda: None)()


def reduce(element, f):
    it = getattr(element, "children", lambda: iter(()))()
    return f(element, (reduce(sub, f) for sub in it) )


def non_recursive_travers(elem, f):
    stack = [(elem, True, None)]

    while stack:
        elem, isFirst, it = stack.pop()
        if it is None:
            if f(elem) and isinstance(elem, AST):
                it = iter(elem)
            else:
                it = iter(())
            stack.append((elem, isFirst, it))
        else:
            try:
                nxtElem = next(it)
                stack.append((nxtElem, True, None))

                if isFirst:
                    isFirst = False
                else:
                    getattr(f, "infix", lambda: None)()
                stack.append((elem, isFirst, it))

            except StopIteration:
                getattr(f, "postfix", lambda: None)()


class ASTPrinter:
    def __init__(self):
        self.result = ""
        self.closing = []

    def __call__(self, value):
        if isinstance(value, AST):
            self.result += "(%s " % (value.__class__.__name__)
            self.closing.append(True)
        else:
            self.result += str(value)
            self.closing.append(False)
        return True

    def infix(self):
        self.result += " "

    def postfix(self):
        if self.closing.pop():
            self.result += ")"

class FancyVisitor:
    def __init__(self):
        self.stack = []

    def call(self, prefix, value):
        cls = value.__class__.__name__
        method = "%s_%s"%(prefix,cls.lower())
        method = getattr(self, method, None)
        if method is not None:
            return method(value)
        else:
            method = "%s_%s"%(prefix,"default")
            method = getattr(self, method, None)
            if method is not None:
                return method(value)

    def __call__(self, value):
        self.stack.append(value)
        result = self.call("pre", self.stack[-1])
        return True if result is None else result

    def infix(self):
        self.call("in", self.stack[-1])

    def postfix(self):
        self.call("post", self.stack[-1])
        self.stack.pop()

class FancyReducer:
    def __call__(self, value, childs):
        className = value.__class__.__name__
        method = className.lower()
        method = getattr(self, method, None)
        if method is not None:
            return method(value, childs)
        else:
            method = "default"
            method = getattr(self, method, None)
            if method is not None:
                return method(value, childs)


class ASCIPrinter(FancyVisitor):
    def __init__(self):
        super().__init__()
        self.result = ""

    def pre_add(self, value):
        self.result += "("

    def post_add(self,value):
        self.result += ")"

    def in_add(self, value):
        self.result += " + "

    def in_geq(self, value):
        self.result += " >= "

    def in_mult(self, value):
        self.result += " * "

    def in_implies(self, value):
        self.result += " ==> "

    def in_limplies(self, value):
        self.result += " <== "

    def pre_symbol(self, value):
        self.result += value.v["name"]

    def pre_variable(self, value):
        self.result += value.v["name"]

    def pre_wildcard(self, value):
        self.result += "\\*"

    def pre_int(self, value):
        self.result += str(value)

    def pre_neg(self, value):
        self.result += "~"

def print_asci(ast):
    printer = ASCIPrinter()
    travers(ast, printer)
    print(printer.result)


class AST:
    def __init__(self, names):
        self.order = names
        self.v = dict(zip(names, [None] * len(names)))

    def __eq__(self, other):
        return self.__class__ == other.__class__ \
            and self.v == other.v

    def __iter__(self):
        return (self.v[val] for val in self.order)

    def __getitem__(self, i):
        return self.v[self.order[i]]

    def __setitem__(self, i, val):
        self.v[self.order[i]] = val

    def __bool__(self):
        raise ValueError("Can not convert ast node to bool.")

    def children(self):
        return iter(self)

    def set(self, i, val):
        self.v[self.order[i]] = val

    def match(self, other):
        if self.__class__ != other.__class__:
            return False

        for selfVal, otherVal in zip(self,other):
            try:
                if not selfVal.match(otherVal):
                    return False
            except AttributeError:
                if selfVal != otherVal:
                    return False
        return True

    def __str__(self):
        printer = ASTPrinter()
        travers(self, printer)
        return printer.result
        #return "(%s)"%(",".join(map(str, (self.v[val] for val in self.order))))

    def __add__(self, other):
        return Add([self,other])

    def __mul__(self, other):
        return Mult([self,other])

    def __truediv__(self, other):
        return Div(self,other)

    def __rmul__(self, other):
        return Mult([other, self])

    def __radd__(self, other):
        return Add([other, self])

    def __ge__(self, other):
        return Geq(self, other)

    def __le__(self, other):
        return Leq(self, other)

    def __lt__(self, other):
        return LessThan(self, other)

    def __gt__(self, other):
        return GreaterThan(self, other)

    def __rshift__(self, other):
        return Implies(self, other)

    def __lshift__(self,other):
        return LImplies(self, other)

class LinearSum():
    def __init__(self):
        self.isConstraint = False
        self.isEquailty = False
        self.data = dict()




class Unary(AST):
    def __init__(self, value):
        super().__init__(["value"])
        self.v["value"] = value

    @property
    def value(self):
        return self.v["value"]

    @value.setter
    def value(self, value):
        self.v["value"] = value

class Saturate(Unary):
    pass

class Integer(Unary):
    def __invert__(self):
        if self.value == 1:
            return Integer(0)
        elif self.value == 0:
            return Integer(1)
        else:
            raise ValueError("Can't negate non 0-1 integer.")

class Symbol(AST):
    def __init__(self, name):
        super().__init__(["name"])
        self.v["name"] = name

    @property
    def name(self):
        return self.v["name"]

    @name.setter
    def name(self, value):
        self.v["name"] = value

    def __str__(self):
        return self.name

    def __repr__(self):
        return "Symbol(%s)" % (self.name)

class Variable(Symbol):
    def __init__(self, name):
        super().__init__(name)

    def __invert__(self):
        return Neg(self)

    def __hash__(self):
        return hash(self.name)

    def get_opb_name(self):
        if isinstance(self.name, int):
            return "x%i"%(self.name)
        else:
            return self.name

    def __repr__(self):
        return "Variable(%s)" % (self.name)

class Neg(Unary):
    def __invert__(self):
        return self.v["value"]



class Symbol1(AST):
    def __init__(self, name, idx):
        super().__init__(["name", "idx"])
        self.v["name"] = name
        self.v["idx"] = idx

class BinaryOp(AST):
    def __init__(self, left, right):
        super().__init__(["left", "right"])
        self.v["left"] = left
        self.v["right"] = right

    @property
    def lhs(self):
        return self.v["left"]

    @lhs.setter
    def lhs(self, value):
        self.v["left"] = value

    @property
    def rhs(self):
        return self.v["right"]

    @rhs.setter
    def rhs(self, value):
        self.v["right"] = value

class Root(AST):
    def __init__(self, ast):
        super().__init__(["ast"])
        self.v["ast"] = ast

class Constraint(BinaryOp):
    pass

class Geq(Constraint):
    isGeq = True

class LessThan(Constraint):
    pass

class GreaterThan(Constraint):
    pass

class Leq(Constraint):
    isLeq = True

class Equals(Constraint):
    isEq = True

    def getGeq(self):
        result = Geq(self.lhs, self.rhs)
        try:
            result.constraintId = self.constraintId[0]
        except AttributeError:
            pass
        return result

    def getLeq(self):
        result = Leq(self.lhs, self.rhs)
        try:
            result.constraintId = self.constraintId[1]
        except AttributeError:
            pass
        return result

eq = Infix(lambda x,y: Equals(x,y))

class Implies(BinaryOp):
    pass

class LImplies(BinaryOp):
    pass

class NaryOp(AST):
    def __init__(self, values):
        self.values = values

    def __eq__(self, other):
        return self.__class__ == other.__class__ \
            and self.values == other.values

    def __iter__(self):
        return iter(self.values)

    def __getitem__(self, i):
        return self.values[i]

    def __setitem__(self, i, val):
        self.values[i] = val

    def __len__(self):
        return len(self.values)

class Mult(NaryOp):
    pass

class Div(BinaryOp):
    pass

class Add(NaryOp):
    pass

class WildCard(AST):
    def __init__(self):
        super().__init__([])
        self.val = None

    def match(self, other):
        if self.val is None:
            self.val = other
            return True
        else:
            return self.val == other

    def transform(self):
        assert(self.val is not None)
        return self.val

def resetWildCard(node):
    if isinstance(node, WildCard):
        node.val = None
    return True

def resetWildCards(node):
    travers(node, resetWildCard)

class Contains(AST):
    def __init__(self, cls, remainder, toMatch):
        self.cls = cls
        self.remainder = remainder
        self.toMatch = toMatch
        self.val = None

    def __iter__(self):
        return iter([self.remainder] + self.toMatch)

    def __getitem__(self, i):
        if (i == 0):
            return self.remainder
        else:
            return self.toMatch[i - 1]

    def __setitem__(self, i, val):
        if (i == 0):
            self.remainder = val
        else:
            self.values[i - 1] = val

    def match(self, other):
        remaining = list(self.toMatch)
        matched = set()

        if isinstance(other, self.cls):
            for i, child in enumerate(other):
                for val in remaining:
                    if val.match(child):
                        matched.add(i)
                        remaining.remove(val)
                        break

            if len(remaining) == 0:
                self.remainder.val = self.cls([child for i, child in enumerate(other) if i not in matched])
                return True

        return False

class Transformer():
    def __init__(self, method):
        self.method = method

    def transform(self):
        # print("transforming")
        return self.method()


def triggerTransform(node):
    if isinstance(node, AST):
        for i, val in enumerate(node):
            try:
                node[i] = val.transform()
            except AttributeError:
                pass
    return True

def triggerTransforms(node):
    root = Root(node)
    travers(root, triggerTransform)
    return root.v["ast"]

class RewriteState:
    def __init__(self):
        self.changed = False

class RewriteRule:
    def __init__(self, frm, to):
        self.frm = frm
        self.to = to

    def __call__(self, ast, state = RewriteState()):
        resetWildCards(self.frm)
        if self.frm.match(ast):
            res = deepcopy(self.to)
            res = triggerTransforms(res)
            state.changed = True
            return res
        else:
            return ast

    def __str__(self):
        return "RewriteRule" + str(self.frm) + "->" + str(self.to)

class FlattenNary:
    def __call__(self, node, state = RewriteState()):
        if isinstance(node, NaryOp):
            result = []
            for child in node.values:
                if child.__class__ == node.__class__:
                    result.extend(child.values)
                    state.changed = True
                else:
                    result.append(child)
            node.values = result
        return node

def dropUnaryNary(node, state = RewriteState()):
    if isinstance(node, NaryOp) and len(node.values) == 1:
        state.changed = True
        return node.values[0]
    return node

def wrapInteger(node, state):
    if not isinstance(node, Integer) and not isinstance(node, Symbol) and isinstance(node, AST):
        for i, child in enumerate(node):
            if isinstance(child, int):
                state.changed = True
                node[i] = Integer(child)
    return node

class Stats:
    def __init__(self):
        self.rule_checks = 0
        self.rule_applications = 0

    def __str__(self):
        result = ""
        result += "stats: rule_applications: %i \n"%(self.rule_applications)
        result += "stats: rule_checks: %i\n"%(self.rule_checks)
        return result

stats = Stats()


class Rewrite:
    def __init__(self, local_rules = [], toplevel_rules = [], state = RewriteState()):
        self.local_rules = local_rules
        self.toplevel_rules = toplevel_rules
        self.reachedFixpoint = False
        self.outer_state = state
        self.currentRule = None

    def rewrite(self, ast):
        self.reachedFixpoint = False
        root = Root(ast)
        while not self.reachedFixpoint:
            self.reachedFixpoint = True
            for self.currentRule in self.local_rules:
                travers(root, self)

                if not self.reachedFixpoint:
                #     print_asci(root)
                #     print()
                    break

            if self.reachedFixpoint:
                for rule in self.toplevel_rules:
                    state = RewriteState()
                    root = rule(root, state)
                    if state.changed:
                        self.reachedFixpoint = False

            if not self.reachedFixpoint:
                self.outer_state.changed = True

        return root.v["ast"]

    def __call__(self, node):
        global stats
        if isinstance(node, AST):
            for i, val in enumerate(node):
                state = RewriteState()
                stats.rule_checks += 1
                res = self.currentRule(val, state)
                if state.changed:
                    stats.rule_applications += 1
                    #print("rewriting", val, res)
                    self.reachedFixpoint = False
                    node[i] = res
                    # return False

                    # print(self.currentRule)
        return True

class UnnegateVariable:
    def __init__(self, var):
        self.var = var

    def __call__(self, node, state = RewriteState()):
        wilds = [WildCard() for i in range(3)]
        rewriter = Rewrite([
            RewriteRule(
                Geq(
                    Contains(Add, wilds[0], [
                        Contains(Mult, wilds[1], [Neg(self.var)])
                    ]),
                    wilds[2]
                ),
                Geq(
                    Add([
                        wilds[0],
                        Mult([wilds[1], Integer(-1), self.var])
                    ]),
                    Add([
                        Mult([Integer(-1), wilds[1]]),
                        wilds[2]
                    ])
                )
            ),
        ], state = state)
        return rewriter.rewrite(node)

class NegateVariable:
    def __init__(self, var):
        self.var = var

    def __call__(self, node, state = RewriteState()):
        wilds = [WildCard() for i in range(3)]
        rewriter = Rewrite([
            RewriteRule(
                Geq(
                    Contains(Add, wilds[0], [
                        Contains(Mult, wilds[1], [self.var])
                    ]),
                    wilds[2]
                ),
                Geq(
                    Add([
                        wilds[0],
                        Mult([wilds[1], Integer(-1), Neg(self.var)])
                    ]),
                    Add([
                        Mult([Integer(-1), wilds[1]]),
                        wilds[2]
                    ])
                )
            ),
        ], state = state)
        return rewriter.rewrite(node)

def multiplyOut(ast, state = RewriteState()):
    wilds = [WildCard() for i in range(2)]

    rule = RewriteRule(
        Contains(Mult, wilds[0],[
            Contains(Add, wilds[1], []),
        ]),
        Mult([wilds[0], wilds[1]])
    )
    tmpState = RewriteState()
    ast = rule(ast, tmpState)

    if not tmpState.changed:
        return ast
    else:
        mult = ast.values[0]
        add  = ast.values[1]
        if len(add) > 0:
            state.changed = True

            for i, val in enumerate(add):
                add[i] = deepcopy(mult) * val

        return add

    return ast

def simplify(ast, state = RewriteState()):
    wilds = [WildCard() for i in range(10)]

    rewriter = Rewrite([
        wrapInteger,
        FlattenNary(),
        dropUnaryNary,

        # pull variables together
        RewriteRule(
            Contains(Add, wilds[0],[
                Contains(Mult, wilds[1], [Variable(wilds[2])]),
                Contains(Mult, wilds[3], [Variable(wilds[2])]),
            ]),
            Add([
                wilds[0],
                Mult([
                    Add([wilds[1], wilds[3]]),
                    Variable(wilds[2])
                ])
            ])
        ),

        # add integers
        RewriteRule(
            Contains(Add, wilds[0],[
                Integer(wilds[1]),
                Integer(wilds[2])
            ]),
            Add([
                wilds[0],
                Transformer(lambda: Integer(wilds[1].val + wilds[2].val))
            ])
        ),

        # multiply integers
        RewriteRule(
            Contains(Mult, wilds[0],[
                Integer(wilds[1]),
                Integer(wilds[2])
            ]),
            Mult([
                wilds[0],
                Transformer(lambda: Integer(wilds[1].val * wilds[2].val))
            ])
        ),

        # multiplication by 0
        RewriteRule(
            Contains(Mult, wilds[0],[
                Integer(0)
            ]),
            Integer(0)
        ),

        # addition by 0
        RewriteRule(
            Contains(Add, wilds[0],[
                Integer(0)
            ]),
            Add([wilds[0]])
        ),

        multiplyOut
    ], state = state)

    return rewriter.rewrite(ast)

def isLiteral(ast):
    return isinstance(ast, Variable) or (isinstance(ast, Neg) and isinstance(ast.value, Variable))

def removeNegativeCoefficients(C, state = RewriteState(), strict = False):
    """
    Tries to remove negative coefficients in the left hand side of a
    greater equal constraint by negating the literal and increasing
    the right hand side.
    """

    if not isinstance(C, Geq):
        if strict:
            raise ValueError("not geq")
        else:
            return C

    terms  = C.lhs

    if isinstance(terms, Add):
        values = terms.values
    else:
        values = [terms]

    for term in values:
        if isinstance(term, Mult) and len(term.values) == 2:
            coeff = term.values[0]
            lit   = term.values[1]

            if isinstance(coeff, Integer) and isLiteral(lit):
                if coeff.value < 0:
                    state.changed = True
                    coeff.value *= -1
                    C.rhs = C.rhs + coeff.value
                    term.values[1] = ~lit


            elif strict:
                raise ValueError("no int or literal in term")
        elif strict:
            raise ValueError("no mult or non binary mult")

    return C

def replaceNegatedConstraint(ast, state = RewriteState()):
    if isinstance(ast, Neg) and isinstance(ast.value, Geq):
        C = ast.value
        C.degree = -1 * C.degree + 1



def removeImplication(impl, state = RewriteState()):
    if isinstance(impl, Implies):
        if isLiteral(impl.lhs):
            try:
                C = removeNegativeCoefficients(impl.rhs, strict = True)
            except ValueError as e:
                return impl

            lit = impl.lhs


            C.lhs = C.lhs + deepcopy(C.rhs) * (~lit)

            state.changed = True
            return C

    elif isinstance(impl, LImplies):
        if isLiteral(impl.lhs):
            C = Neg(impl.rhs)
            C = normalize0(C, state)
            try:
                C = removeNegativeCoefficients(C, strict = True)
            except ValueError:
                return impl

            lit = impl.lhs
            C.lhs = C.lhs + deepcopy(C.rhs) * lit

            state.changed = True
            return C

    return impl


def addUnaryGeq(node, state = RewriteState()):
    if isinstance(node, Geq) and not isinstance(node.lhs,Add):
        state.changed = True
        node.lhs = Add([node.lhs])
    return node

def normalize0(ast, state = RewriteState()):
    wilds = [WildCard() for i in range(10)]

    phase1 = Rewrite([
        # replace negated geq
        RewriteRule(
            Neg(Geq(wilds[0], wilds[1])),
            LessThan(wilds[0], wilds[1])
        ),

        # replace less than
        RewriteRule(
            LessThan(wilds[0], wilds[1]),
            Leq(wilds[0], wilds[1] + -1)
        ),

        # replace leq
        RewriteRule(
            Leq(wilds[0], wilds[1]),
            Geq(-1 * wilds[0], -1 * wilds[1]),
        ),

        # shift integer to lhs of mult
        RewriteRule(
            Mult([Variable(wilds[0]), Integer(wilds[1])]),
            Mult([Integer(wilds[1]), Variable(wilds[0])]),
        ),

        # shift integer to rhs of geq
        RewriteRule(
            Geq(Contains(Add, wilds[0], [Integer(wilds[1])]), wilds[2]),
            Geq(wilds[0], Integer(-1) * Integer(wilds[1]) + wilds[2])
        ),

        # shift integer to rhs of geq
        RewriteRule(
            Geq(Integer(wilds[1]), wilds[2]),
            Geq(Add([]), Integer(-1) * Integer(wilds[1]) + wilds[2])
        ),

        # add coefficient to variable
        RewriteRule(
            Contains(Add, wilds[0], [Variable(wilds[1])]),
            Add([
                wilds[0],
                Mult([
                    Integer(1),
                    Variable(wilds[1])
                ])
            ])
        ),

        # add coefficient to negated variable
        RewriteRule(
            Contains(Add, wilds[0], [Neg(Variable(wilds[1]))]),
            Add([
                wilds[0],
                Mult([
                    Integer(1),
                    Neg(Variable(wilds[1]))
                ])
            ])
        ),

        # add coefficient to variable
        RewriteRule(
            Geq(Variable(wilds[0]), wilds[1]),
            Geq(Mult([
                    Integer(1),
                    Variable(wilds[0])
                ]), wilds[1]),
        ),

        # add coefficient to negated variable
        RewriteRule(
            Geq(Neg(Variable(wilds[0])), wilds[1]),
            Geq(Mult([
                    Integer(1),
                    Neg(Variable(wilds[0]))
                ]), wilds[1]),
        ),

        UnnegateVariable(Variable(wilds[0])),

        RewriteRule(
            Geq(Variable(wilds[0]), wilds[1]),
            Geq(Mult([Integer(1), Variable(wilds[0])]), wilds[1])
        ),
    ],
    toplevel_rules = [simplify],
    state = state)

    return phase1.rewrite(ast)

def normalize(ast, state = RewriteState()):
    wilds = [WildCard() for i in range(10)]

    phase0 = Rewrite(
        local_rules = [removeImplication],
        toplevel_rules = [normalize0],
        state = state
    )

    phase1 = Rewrite(
        local_rules = [removeNegativeCoefficients],
        toplevel_rules = [simplify],
        state = state
    )

    phase2 = Rewrite(
        local_rules = [addUnaryGeq]
    )

    ast = phase0.rewrite(ast)
    ast = phase1.rewrite(ast)
    ast = phase2.rewrite(ast)
    return ast

@TimedFunction.time("normalize_by_reduce")
def normalize_by_reduce(ast):
    def negate(constr):
        # for a constraint (>=, <= or =) we get {None: -rhs, x_i:
        # a_i} meaning sum(a_i * x_i) >= rhs and want to produce
        # sum(-a_i * x_i) >= -rhs + 1, i.e., {None: rhs - 1, x_i:
        # -a_i}

        for key, value in constr.items():
            constr[key] = -1 * value

        constr[None] = constr.get(None, 0) - 1
        return constr

    def computeNormalizedRhs(constr):
        result = 0
        for key, value in constr.items():
            if key is None:
                result -= value
            else:
                if value < 0:
                    result -= value
        return result

    def litImpliesConstr(lit, constr):
        result = constr
        coeff = computeNormalizedRhs(constr)
        if isinstance(lit, Neg):
            var = lit.value
            result[var] = result.get(var, 0) + coeff
        else:
            var = lit
            result[var]  = result.get(var, 0) - coeff
            result[None] = result.get(None, 0) + coeff

        return result


    def constrImpliesLit(constr, lit):
        return litImpliesConstr(Neg(lit), negate(constr))


    def geqCombine(lhs, rhs):
        result = lhs
        for key, val in rhs.items():
            result[key] = result.get(key, 0) + (-1 * val)

        return result

    class Normalizer(FancyReducer):
        # The accumulator contains a dict from variables to
        # coefficients, where None maps to the constant part of the
        # constraint. We think of all coefficients and the constant of
        # beeing on the left-hand-side of an '>=' constraint.

        def add(self, node, childs):
            result = dict()
            for child in childs:
                for key, val in child.items():
                    result[key] = val + result.get(key, 0)
            return result

        def mult(self, node, childs):
            factor = 1
            result = None
            for child in childs:
                if None in child and len(child) == 1:
                    factor = factor * child[None]
                else:
                    if result is not None:
                        raise NotImplementedError("Can not handle non linear constraints.")
                    result = child

            # no variable found
            if result is None:
                result = {None: 1}

            for key, val in result.items():
                result[key] = val * factor

            return result

        def variable(self, node, childs):
            return { node: 1 }

        def integer(self, node, childs):
            return { None: node.value }

        def int(self, node, childs):
            return { None: node }

        def neg(self, node, childs):
            result = next(childs)


            if isinstance(node.value, Variable):
                for key, value in result.items():
                    result[key] = -1 * value
                # for Neg(Variable(x)) we get {Variable(x): 1} and want to
                # produce  {Variable(x): -1, None: 1}
                result[None] = result.get(None, 0) + 1

            elif isinstance(node.value, Constraint):
                result = negate(result)
            else:
                raise ValueError("Can not negate given node.")

            return result

        def implies(self, node, childs):
            lhs = next(childs)
            rhs = next(childs)

            if isLiteral(node.lhs):
                return litImpliesConstr(node.lhs, rhs)
            elif isLiteral(node.rhs):
                return constrImpliesLit(lhs, node.rhs)
            else:
                ValueError("Can only normalize implication with literal on one side.")

        def limplies(self, node, childs):
            lhs = next(childs)
            rhs = next(childs)

            if isLiteral(node.rhs):
                return litImpliesConstr(node.rhs, lhs)
            elif isLiteral(node.lhs):
                return constrImpliesLit(rhs, node.lhs)
            else:
                ValueError("Can only normalize implication with literal on one side.")

        def default(self, node, childs):
            raise NotImplementedError("Cant normalize %s"%(str(node)))
            try:
                child = next(childs)
                return child
            except StopIteration:
                return dict()

        def geq(self, node, childs):
            lhs = next(childs)
            rhs = next(childs)
            return geqCombine(lhs, rhs)

        def leq(self, node, childs):
            rhs = next(childs)
            lhs = next(childs)
            return geqCombine(lhs, rhs)

        def lessthan(self, node, childs):
            rhs = next(childs)
            lhs = next(childs)
            rhs[None] = rhs.get(None, 0) + 1
            return geqCombine(lhs, rhs)

        def greaterthan(self, node, childs):
            lhs = next(childs)
            rhs = next(childs)
            rhs[None] = rhs.get(None, 0) + 1
            return geqCombine(lhs, rhs)


    if isinstance(ast, Equals):
        isEq = True
        ast = ast.lhs >= ast.rhs
    else:
        isEq = False

    reducer = Normalizer()
    reduced = reduce(ast, reducer)

    terms = []
    degree = 0
    for key, value in reduced.items():
        if key is None:
            degree = degree + -1 * value
        else:
            if value < 0:
                terms.append( -value * ~key )
                degree = degree - value
            elif value > 0:
                terms.append(  value * key )

    if not isEq:
        return Add(terms) >= degree
    else:
        return Add(terms) |eq| degree

def list_variables(ast):
    class VarFinder(FancyVisitor):
        def __init__(self):
            super().__init__()
            self.result = []

        def pre_variable(self,value):
            self.result.append(value)

    visitor = VarFinder()
    travers(ast, visitor)
    return visitor.result

def collect_variables(ast):
    class VarFinder(FancyVisitor):
        def __init__(self):
            super().__init__()
            self.result = set()

        def pre_variable(self,value):
            self.result.add(value)

    visitor = VarFinder()
    travers(ast, visitor)
    return visitor.result

class Variable2IntRenaming():
    def __init__(self):
        self.variableNames = dict()
        self.nextVarNum = 1

    def __call__(self, ast):
        class VarFinder(FancyVisitor):
            def __init__(self, master):
                super().__init__()
                self.master = master

            def pre_variable(self,value):
                value.name = self.master.variableNames.setdefault(value.name, self.master.nextVarNum)
                if value.name == self.master.nextVarNum:
                    self.master.nextVarNum += 1

        ast = deepcopy(ast)
        visitor = VarFinder(self)
        travers(ast, visitor)
        return ast



def main():
    x = Variable("x")
    y = Variable("y")
    z = Variable("z")

    a = Symbol("a")
    b = Symbol("b")
    c = Symbol("c")

    # D = Neg(y + x >= 2)
    # D = normalize(D)
    # print_asci(D)


    # C = z << (y + x >= 2)
    # print_asci(C)
    # C = normalize(C)
    # print_asci(C)

    # print_asci(normalize(~x >= 1))

    C = normalize_by_reduce( x + y + z <= 1 )
    print(C)
    print_asci(C)


if __name__ == '__main__':
    main()

