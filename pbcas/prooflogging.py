from pbcas.opb_formula import asOPB
from pbcas.ast import *
from pbcas.timed_function import TimedFunction

class PlainProof(object):
    """
    Abstraction layer for translating the available rules to their
    ASCII representation.
    """

    def __init__(self, _proof_file, numFormulaConstraints, rename = lambda x: x):
        self.proof_file = _proof_file
        print("pseudo-Boolean proof version 1.2", file=self.proof_file)
        print("f %i 0"%(numFormulaConstraints), file=self.proof_file)
        self.num_constraints = numFormulaConstraints
        self.rename = rename

    def _new_constraint_id(self):
        self.num_constraints += 1
        return self.num_constraints

    def add_redundant(self, constraint, wittnes):
        """
        :param clause: clause to add, similar to drat we assume that the
            first variable is the 'pivot' variable, e.g., the fresh
            variable.
        """

        print("red", asOPB(self.rename(constraint)),  " ".join(("%s -> %s"%(self.rename(var).get_opb_name(), str(val)) for var, val in wittnes)),
            file = self.proof_file)

        return self._new_constraint_id()

    def add_rup(self, constraint):
        print("u %s"%(asOPB(self.rename(constraint))) , file = self.proof_file)

        return self._new_constraint_id()

    def add_assume(self, constraint):
        print("a %s"%(asOPB(self.rename(constraint))) , file = self.proof_file)

        return self._new_constraint_id()

    def equals(self, constraint_id, constraint):
        print("e %i %s"%(constraint_id, asOPB(self.rename(constraint))), file=self.proof_file)

    def implies(self, constraint_id, constraint):
        print("i %i %s"%(constraint_id, constraint), file=self.proof_file)

    def set_level(self, level):
        print("# %i"%(level), file=self.proof_file)

    def wipe_level(self, level):
        print("w %i"%(level), file=self.proof_file)

    def delete(self, constraintIds):
        print("del id %s"%(" ".join( (str(cid) for cid in constraintIds))  ), file=self.proof_file)

    def comment(self, *args):
        if len(args) == 0:
            print("", file=self.proof_file)
        else:
            print("*", *args, file=self.proof_file)

class PolishNotationStep(object):
    """
    Abstraction layer for translating the reverse polish notation rule
    to its ASCII representation.
    """

    def __init__(self, proof):
        self.proof_file = proof.proof_file
        self.id = proof._new_constraint_id()
        print("p", end=" ", file=self.proof_file)

    def finalize(self):
        print("", file=self.proof_file)

    def multiply(self, m):
        assert(m >= 0)
        if m != 1:
            print("%i *"%(m), end=" ", file=self.proof_file)

    def add(self):
        print("+", end=" ", file=self.proof_file)

    def div_and_round(self, m):
        assert(m >= 0)
        if m != 1:
            print("%i d"%(m), end=" ", file=self.proof_file)

    def saturate(self):
        print("s", end=" ", file=self.proof_file)

    def load_constraint(self, constraintId):
        print(constraintId, end=" ", file=self.proof_file)

    def load_literal_axiom(self, literal):

        if isinstance(literal, Neg):
            print("~", end="", file=self.proof_file)
            literal = literal.value

        print(self.proof.rename(literal).get_opb_name(), end=" ", file=self.proof_file)


def naryToBinaryAdd(ast, state = RewriteState()):
    if isinstance(ast, Add) and len(ast) > 2:
        state.changed = True

        it = iter(ast)
        result = Add([next(it), next(it)])
        for node in it:
            result = Add([result, node])

        return result

    return ast

class Proof():
    def __init__(self, proof_file, num_constraints, rename):
        self.proof = PlainProof(proof_file, num_constraints, rename)

    @property
    def rename(self):
        return self.proof.rename

    @rename.setter
    def rename(self, value):
        self.proof.rename = value


    @TimedFunction.time("reification")
    def reification(self, ast):
        """
            Proof log a proof either for an ast of the form variable
            ==> constraint or variable <== constraint. The given ast
            node is anotated with the constraintId given by the proof.

        """
        class ProofReify(FancyVisitor):
            def __init__(self, proof):
                super().__init__()
                self.proof = proof

            def pre_implies(self,value):
                witness = [(value.lhs, 0)]
                value.constraintId = self.proof.add_redundant(value, witness)

            def pre_limplies(self,value):
                witness = [(value.lhs, 1)]
                value.constraintId = self.proof.add_redundant(value, witness)

            def pre_default(self, value):
                implyingLit = getattr(value, "implyingLit", None)
                if implyingLit is not None:
                    if isinstance(implyingLit, Neg):
                        witness = [(implyingLit.value, 1)]
                    else:
                        witness = [(implyingLit, 0)]
                    value.constraintId = self.proof.add_redundant(value, witness)
                else:
                    raise ValueError("Expected implication.")



        f = ProofReify(self.proof)
        f(ast)
        return ast

    @TimedFunction.time("derivation")
    def derivation(self, ast):
        """
            Proof log a cutting planes derivation, i.e., translates
            the given ast containing operation on constraints into
            reverse polish notation as required by the proof format.
            If a node is anotated with a constraintId the ast is not
            traversed further but instead the constraintId is used.

            The given root node is anotated with the constraintId
            given by the proof.

        """
        class ProofPolish(FancyVisitor):
            def __init__(self, step):
                super().__init__()
                self.step = step

            def post_add(self, value):
                if not hasattr(value, "constraintId"):
                    if value[0] != 0 and value[1] != 0:
                        self.step.add()

            def int(self, value):
                if value != 0:
                    raise ValueError("Found integer in derivation.")

            def post_mult(self, value):
                if not hasattr(value, "constraintId"):
                    if not len(value) == 2:
                        raise ValueError("Can not log multiplication with more than two operators.")
                    else:
                        if isinstance(value[0], int):
                            val = value[0]
                        elif isinstance(value[1], int):
                            val = value[1]
                        else:
                            raise ValueError("Can not log multiplication with non integer.")

                        self.step.multiply(val)

            def post_div(self, value):
                if not hasattr(value, "constraintId"):
                    if isinstance(value.rhs, int):
                        val = value.rhs

                    else:
                        raise ValueError("Can not log division with non integer.")

                    self.step.div_and_round(val)

            def post_saturate(self, value):
                self.step.saturate()

            def pre_default(self, value):
                constraintId = getattr(value, "constraintId", None)
                if constraintId is not None:
                    self.step.load_constraint(constraintId)
                    return False
                else:
                    return True


        step = PolishNotationStep(self.proof)

        visitor = ProofPolish(step)
        travers(ast, visitor)
        step.finalize()
        ast.constraintId = step.id
        return ast

    def rup(self, constraint):
        """
            Proof log a constraint that is RUP (reverse unit
            propagation).

            The given root node is anotated with the constraintId
            given by the proof.
        """

        cId = self.proof.add_rup(constraint)
        constraint.constraintId = cId
        return constraint

    def assume(self, constraint):
        """
            The given root node is anotated with the constraintId
            given by the proof.
        """

        cId = self.proof.add_assume(constraint)
        constraint.constraintId = cId
        return constraint

    def equals(self, constraint_id, constraint):
        return self.proof.equals(constraint_id, constraint)

    def set_level(self, level):
        self.proof.set_level(level)

    def wipe_level(self, level):
        self.proof.wipe_level(level)

    def delete(self, constraints):
        self.proof.delete( (C.constraintId for C in constraints) )

    def comment(self, *args):
        self.proof.comment(*args)
