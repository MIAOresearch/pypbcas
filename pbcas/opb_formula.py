from pbcas.ast import *
from copy import deepcopy
from pbcas.timed_function import TimedFunction

class InvalidNode(RuntimeError):
    pass

class OPBPrinter(FancyVisitor):
    def __init__(self):
        super().__init__()
        self.result = ""

    def in_geq(self, value):
        self.result += " >= "

    def in_equals(self, value):
        self.result += " = "

    def in_mult(self, value):
        self.result += " "

    def in_add(self, value):
        self.result += " "

    def pre_variable(self, value):
        self.result += value.get_opb_name()
        return False

    def pre_neg(self, value):
        self.result += "~"

    def pre_geq(self, value):
        pass

    def pre_equals(self, value):
        pass

    def pre_add(self, value):
        pass

    def pre_mult(self, value):
        pass

    def pre_integer(self, value):
        self.result += str(value.value)
        return False

    def pre_int(self, value):
        self.result += str(value)
        return False

    def pre_default(self, value):
        print(value.__class__.__name__)
        raise InvalidNode()

    #todo: produce error on invalid nodes

@TimedFunction.time("asOPB")
def asOPB(ast, isObjective = False):
    if hasattr(ast, "asOPB"):
        return ast.asOPB(isObjective)

    # ast = normalize(deepcopy(ast))
    if not getattr(ast, "isNormalized", False):
        ast = normalize_by_reduce(ast)
    # print_asci(ast)
    # print(ast)
    visitor = OPBPrinter()
    if isObjective:
        if isinstance(ast, Add):
            travers(ast, visitor)
        else:
            travers(ast[0], visitor)
    else:
        travers(ast, visitor)
    # print(visitor.result)
    return visitor.result + " ;"

class OPBFormula(NaryOp):
    def __init__(self, constraints = None, objective = None, rename = lambda x: x):
        if constraints is None:
            constraints = []
        super().__init__(constraints)
        self.rename = rename
        self.numInequalities = 0
        self.objective = objective

    def add(self, constraint):
        self.values.append(constraint)
        if isinstance(constraint, Equals):
            self.numInequalities += 2
        else:
            self.numInequalities += 1

    def add_all(self, constraints):
        for C in constraints:
            self.add(C)

    def write(self, file, startConstraintId = 1):
        numConstraints = len(self.values)
        numVars = len(collect_variables(self))

        print("* #variable= %i #constraint= %i"%(numVars, numConstraints), file=file)

        if self.objective is not None:
            print("min:", asOPB(self.rename(self.objective), isObjective = True), file = file)

        constraintId = startConstraintId
        for constraint in self.values:
            print(asOPB(self.rename(constraint)), file = file)
            if constraintId is not None:
                if isinstance(constraint, Equals):
                    constraint.constraintId = (constraintId, constraintId + 1)
                    constraintId += 2
                else:
                    constraint.constraintId = constraintId
                    constraintId += 1

    def num_inequalities(self):
        return self.numInequalities

    def num_constraints(self):
        return len(self.values)

    def getConstraints(self):
        return self.values

    def __str__(self):
        result = ""
        for constraint in self.values:
            result += str(constraint) + "\n"
        return result
